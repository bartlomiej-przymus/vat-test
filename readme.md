## Vat calculator test

I have decided to create this project in latest Symfony 6.3.
Prior to this I have never wrote symfony project.

To set up please clone repo run `composer install`

Install npm dependencies by running `npm install`

Build css classes by running: `npm run dev`

Please set up database connection in .env file.

The repository should have all the commits available to check what changes have been done.
