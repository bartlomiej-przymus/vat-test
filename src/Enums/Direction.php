<?php

namespace App\Enums;

enum Direction: string {
    case NetToGross =  'net_to_gross';
    case GrossToNet =  'gross_to_net';
}