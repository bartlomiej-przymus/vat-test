<?php

namespace App\Controller;

use App\Entity\History;
use App\Enums\Direction;
use App\Form\Type\CalcType;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class HomeController extends AbstractController
{
    /**
     * @throws ORMException
     */
    #[Route('/', name: 'app_home', methods: ['GET', 'POST'])]
    public function index(
        EntityManagerInterface $entityManager,
        HistoryRepository $repository,
        Request $request): Response
    {
        $form = $this->createForm(CalcType::class, new History());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $history = $form->getData();
            $value = $history->getValue();
            $rate = $history->getRate();

            $tax = ($rate / 100) * $value;

            $history->setDirection(Direction::NetToGross->value)
                ->setNetValue($value)
                ->setGrossValue(round($value + $tax, 2));

            $entityManager->persist($history);

            $entityManager->flush();

            $history = (new History)
                ->setRate($rate)
                ->setValue($value)
                ->setDirection(Direction::GrossToNet->value)
                ->setNetValue(round($value - $tax, 2))
                ->setGrossValue($value);

            $entityManager->persist($history);

            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        $history = $repository->findAll();

        return $this->render('home/index.html.twig', [
            'form' => $form,
            'history' => $history,
        ]);
    }

    #[Route('/clear', name: 'clear', methods: ['GET'])]
    public function clear(EntityManagerInterface $entityManager,): RedirectResponse
    {
        $query = $entityManager->createQuery(
            'DELETE FROM App\Entity\History e'
        )->execute();

        return $this->redirectToRoute('app_home');
    }

    #[Route('/export', name: 'export')]
    public function export(
        HistoryRepository $repository,
    ): Response {
        $history = $repository->findAll();

        if (empty($history)) {
            return $this->redirectToRoute('app_home');
        }

        $encoders = [new CsvEncoder()];
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $csvContent = $serializer->serialize($history, 'csv');

        $response = new Response($csvContent);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=sample.csv');

        return $response;
    }
}
