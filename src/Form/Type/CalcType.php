<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;

class CalcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('value', MoneyType::class, [
                'currency' => '',
                'constraints' => [
                    new Positive(),
                ],
            ])
            ->add('rate', PercentType::class, [
                'type' => 'integer',
                'constraints' => [
                    new Positive(),
                    new LessThanOrEqual(100),
                ],
            ])
            ->add('calculate', SubmitType::class);
    }
}
