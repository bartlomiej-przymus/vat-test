<?php

namespace App\Entity;

use App\Repository\HistoryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HistoryRepository::class)]
class History
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $net_value = null;

    #[ORM\Column]
    private ?float $gross_value = null;

    #[ORM\Column(length: 12)]
    private ?string $direction = null;

    #[ORM\Column]
    private ?int $rate = null;

    #[ORM\Column]
    private ?float $value = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNetValue(): ?float
    {
        return $this->net_value;
    }

    public function setNetValue(float $net_value): static
    {
        $this->net_value = $net_value;

        return $this;
    }

    public function getGrossValue(): ?float
    {
        return $this->gross_value;
    }

    public function setGrossValue(float $gross_value): static
    {
        $this->gross_value = $gross_value;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): static
    {
        $this->direction = $direction;

        return $this;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): static
    {
        $this->rate = $rate;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): static
    {
        $this->value = $value;

        return $this;
    }
}
